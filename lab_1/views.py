from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name1 = 'Revan Ragha Andhito'
birth_date1 = date(1999,4,15)
npm1 = 1706039420
mhs_name2 = ' Muhammad Farras Hakim'
birth_date2 = date(1999,7,1)
npm2 =  1706024513
mhs_name3 = 'Nur Nisrina Ningrum'
npm3 =  1706979423
curr_year = int(datetime.now().strftime("%Y"))

# Create your views here.
def index(request):
    response = {'name1': mhs_name1, 'age1': calculate_age(birth_date1.year), 'npm1': npm1,
                'name2': mhs_name2, 'age2': calculate_age(birth_date2.year), 'npm2': npm2,
                'name3': mhs_name3, 'npm3': npm3}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
